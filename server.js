const express = require('express');
const path = require('path');
const webpush = require('web-push');
const cookieParser = require('cookie-parser');
const publicKey = 'BJq-zSro2UQ9sNK2CyGsJZ_uOzEVwgLPRKDox3Eigesmo_a_Jwjbvy4-B7D8Tie8jmE2xyZGMwsU0q21G_oLaxY';
const privateKey = 'zWtSecRbb8ALRO3m6ZrFmOvnBNK5P82zpSQg1n65UwY';
// const vapidKeys = webpush.generateVAPIDKeys();
const gcmAPIKey = 'AIzaSyB8QQf-CMv-xhhq0Fli17V3BHdzcGo85_8';
// gcmAPIKey: gcmAPIKey,
const options = {
    vapidDetails: {
        subject: 'mailto:ravikiran.j@way2online.co.in',
        publicKey: publicKey,
        privateKey: privateKey
    },
    TTL: 60
};

let debug = false;
if (process.env.debug) debug = true;

const log = (...data) => {
    if (debug === true) console.log(data);
};



let subs = [];

let pushMessage = (msg, cb) => {
    msg = msg || 'Hello babe';
    let pushSubscription = { "endpoint": "https://fcm.googleapis.com/fcm/send/cuBCPhETPVY:APA91bE3i2f35TC_7waC1RY5fZcd0hevz8CllkDCUvtnIVvC9gGpM1o4jgwd6g8WmSbmurA7dLaVpicCNdhOCznc7MqwxnQVSG6CyhwcBfTGX-XlstzP9hGcRRZ1E3JXi-IzT57BSux1", "expirationTime": null, "keys": { "p256dh": "BG2zJT2_NCvUDzR2FxW43dTbr2rr-0IXEuY1IkQr68AtCAv3akED0vEHP-g9Ae6PB46O1kpZ9RT0SMP3C9iA9Ms", "auth": "12LlEHEwJTJKss1o5c3n3A" } };
    // pushSubscription.endpoint = subs[0].endpoint;

    pushSubscription = subs[0] || pushSubscription;
    // console.log(JSON.stringify(pushSubscription));
    // log(pushSubscription);

    let payload = JSON.stringify({
        title: 'Push from node',
        body: msg,
        icon: '/images/icon.png',
        url: 'https://example.com'
    });

    try {
        webpush.sendNotification(
            pushSubscription,
            payload,
            options
        ).then(resp => {
            cb(null, resp);
        }).catch(err => {
            log(err);
            cb(err, null);
        });
    } catch (error) {
        log(error);
    }
};

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static('public'));

app.get('/', (req, res) => {
    let filePath = path.join(__dirname, '/index.html');
    res.sendFile(filePath);
});

app.post('/push', (req, res) => {
    pushMessage('Hello tommy', (err, status) => {
        res.send({ error: !!status });
    });
    // res.send({ error: false });
    // pushMessage('Hello tommy', (err, status) => {
    //     log(err, status);
    //     res.send({ error: !!status });
    // });
});

app.post('/subscription', (req, res) => {
    subs.push(req.body);
    res.send({ error: false });
});

// app.get('/public-key', (req, res) => {
//     res.send({ 'key': vapidKeys.publicKey });
// });

app.listen(8080, (err, status) => {
    console.log('listening on 8080');
});